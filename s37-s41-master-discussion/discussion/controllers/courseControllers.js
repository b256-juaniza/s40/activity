const Course = require("../models/Course.js");


// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (course) => {

	let newCourse = new Course({
		name: course.name,
		description: course.description,
		price: course.price
	});

		return newCourse.save().then((result, err) => {

			if(err) {
				console.log(err);
				return false;

			} else {

				return true;
				
			}
		})
};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqparams) => {
	return Course.findById(reqparams.courseId).then((result, err) => {
		if(err) {
			return err;
		} else {
			return result;
		}
	})
}

module.exports.updateCourse = (paramsId, course) => {

	let updatedCourse = {
		name : course.name,
		description : course.description,
		price: course.price
	}

	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveCourse = (paramsId, course) => {

	return Course.findByIdAndUpdate(paramsId.courseId, {isActive : course.isActive}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

